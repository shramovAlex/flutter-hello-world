import 'dart:math';

import 'dart:ui';

import 'package:flutter/material.dart';

class Utils {
  static Color generateRandomColor() {
    Random random = Random(DateTime.now().millisecondsSinceEpoch);
    return Color.fromARGB(
        255,
        random.nextInt(255),
        random.nextInt(255),
        random.nextInt(255)
    );
  }
}