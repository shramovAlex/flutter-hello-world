import 'package:flutter/material.dart';
import 'package:solidapp/Utils.dart';

import 'NamePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello world!',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(title: 'Hello world'),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Color _randomColor = Utils.generateRandomColor();
  String _name = "";

  void setRandomBackgroundColor() {
    setState(() {
      _randomColor = Utils.generateRandomColor();
    });
  }

  void setName(String text) {
    setState(() {
      _name = text;
    });
  }

  @override
  Widget build(BuildContext context) {

    void navigateToNameRoute(BuildContext context) {
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => NamePage(name: _name))
      );
    }

    Widget _buildSubmitButton() {
      return RaisedButton(
        color: Colors.green,
        onPressed: _name.isNotEmpty ? () => navigateToNameRoute(context) : null,
        child: Text(
          'Submit',
        ),
      );
    }

    return Scaffold(
      backgroundColor: _randomColor,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: InkWell(
          onTap: setRandomBackgroundColor,
          child:Center(
            child:  Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Text(
                    'Hey there, please, input your name below:',
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: TextField(
                      textAlign: TextAlign.center,
                      onChanged: (value) => setName(value),
                    )
                ),
                Container(
                    margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: _buildSubmitButton()
                )
              ],
            ),
          )
      ),
    );
  }
}
