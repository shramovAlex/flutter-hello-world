import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NamePage extends StatefulWidget {
  NamePage({Key key, @required this.name}) : super(key: key);
  final String name;

  @override
  _NamePageState createState() => _NamePageState();
}

class _NamePageState extends State<NamePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Page with name"),
        ),
        body: Center(
          child:  Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: Text(
                  'Hello ' + widget.name + '!',
                ),
              ),
            ],
          ),
        )
    );
  }
}